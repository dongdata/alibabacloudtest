package com.valley.skywalking.shopcommon.domain;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Map;

@MappedSuperclass
public class BaseEntity implements Serializable {
    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @Transient
    private Map<String, Object> params;
}
