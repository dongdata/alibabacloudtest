package com.valley.skywalking.shopcommon.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;


/**
 * @author Administrator
 */
@Entity
@Table(name = "shop_order")
@Data
public class ShopOrder extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long oid;

    //用户id
    private Integer uid;
    //用户名
    private String username;


    //商品id
    private Integer pid;
    //商品名称
    private String pname;
    //商品价格
    private Double pprice;


    //购买数量
    private Integer number;

    private Date orderdate;

}
