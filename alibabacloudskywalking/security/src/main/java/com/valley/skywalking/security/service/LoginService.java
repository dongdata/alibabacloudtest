package com.valley.skywalking.security.service;

import com.valley.skywalking.security.utils.JWTUtil;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
@Component
public class LoginService {
    String header = "Authorization";
    public String  getLoginUserid(HttpServletRequest request)
    {
        // 获取请求携带的令牌
        String token = getToken(request);
        if (token != null && !token.isEmpty())
        {
            try
            {
                String uid = JWTUtil.getUserId(token);
                return uid;
            }
            catch (Exception e)
            {
            }
        }
        return null;
    }


    private boolean isNotEmpty(String token){
        return token != null && !token.isEmpty();
    }

    private String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(header);
//        if (isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX))
//        {
//            token = token.replace(Constants.TOKEN_PREFIX, "");
//        }
        return token;
    }
}
