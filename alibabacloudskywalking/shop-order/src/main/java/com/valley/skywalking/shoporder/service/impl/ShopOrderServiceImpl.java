package com.valley.skywalking.shoporder.service.impl;

import com.valley.skywalking.shopcommon.domain.ShopOrder;
import com.valley.skywalking.shopcommon.page.PageDomain;
import com.valley.skywalking.shopcommon.page.TableDataInfo;
import com.valley.skywalking.shopcommon.service.impl.BaseServiceImpl;
import com.valley.skywalking.shopcommon.utils.StringUtils;
import com.valley.skywalking.shoporder.dao.ShopOrderRepository;
import com.valley.skywalking.shoporder.service.ShopOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Administrator
 */
@Service
public class ShopOrderServiceImpl extends BaseServiceImpl<ShopOrder, ShopOrderRepository> implements ShopOrderService {
    @Resource
    private ShopOrderRepository shopOrderRepository;

    @Override
    public TableDataInfo tableData(ShopOrder shopOrder, PageDomain pageDomain) {
        StringBuilder hql = new StringBuilder(" from ShopOrder where 1=1");
        Map<String, Object> params = new HashMap<String, Object>();
        buildSql(shopOrder, hql, params);
        List<ShopOrder> dataList = shopOrderRepository.list(hql.toString(), params, pageDomain);
        Long total = shopOrderRepository.count(hql.toString(), params);
        return new TableDataInfo(dataList, total);
    }


    private void buildSql(ShopOrder shopOrder, StringBuilder hql, Map<String, Object> params) {

        if (shopOrder.getOid() != null) {
            hql.append(" and oid = :oid");
            params.put("oid", shopOrder.getOid());
        }
        if (StringUtils.isNotEmpty(shopOrder.getPname())) {
            hql.append(" and pname like :pname");
            params.put("pname", "%" + shopOrder.getPname() + "%");
        }
        Optional<Map<String, Object>> opParams = Optional.ofNullable(shopOrder.getParams());
        opParams.ifPresent((lparams) -> {
            String startTime = (String) lparams.get("beginTime");
            if (StringUtils.isNotEmpty(startTime)) {
                hql.append(" and startDate >= date_format(:startDate,' %y%m%d') ");
                params.put("startDate", startTime);
            }
            String endTime = (String) lparams.get("endTime");
            if (StringUtils.isNotEmpty(startTime)) {
                hql.append(" and endTime <= date_format(:endTime,' %y%m%d') ");
                params.put("endTime", endTime);
            }
        });

    }

    @Override
    public void createOrder(ShopOrder order) {
        shopOrderRepository.save(order);
    }

    @Override
    @Transactional
    public void deleteOrder(Long oid){
        shopOrderRepository.deleteById(oid);
    }

}
