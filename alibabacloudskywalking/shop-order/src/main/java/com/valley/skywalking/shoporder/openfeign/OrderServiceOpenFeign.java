package com.valley.skywalking.shoporder.openfeign;

import com.valley.skywalking.shopcommon.domain.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value="shop-product")
public interface OrderServiceOpenFeign {
    @RequestMapping("/product/info/{pid}")
    public Product product(@PathVariable("pid") Integer pid);
}
