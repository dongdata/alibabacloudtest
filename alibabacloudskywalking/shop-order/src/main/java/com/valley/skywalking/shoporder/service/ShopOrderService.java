package com.valley.skywalking.shoporder.service;
import com.valley.skywalking.shopcommon.domain.ShopOrder;
import com.valley.skywalking.shopcommon.page.PageDomain;
import com.valley.skywalking.shopcommon.page.TableDataInfo;
import com.valley.skywalking.shopcommon.service.BaseService;
import com.valley.skywalking.shoporder.dao.ShopOrderRepository;

public interface ShopOrderService extends BaseService<ShopOrder, ShopOrderRepository> {
    TableDataInfo tableData(ShopOrder shopOrder, PageDomain pageDomain);
    void createOrder(ShopOrder order);
    void deleteOrder(Long oid);
}
