package com.valley.skywalking.shoporder;

import com.valley.skywalking.shopcommon.dao.impl.BaseDaoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

/**
 * @author Administrator
 */
@EntityScan(basePackages = {"com.valley.skywalking.shopcommon.domain"})
/* 如果用此注解，需要把所有包路径都包括进去*/
@ComponentScan(basePackages = {"com.valley.skywalking.security", "com.valley.skywalking.shoporder.*"})
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = BaseDaoImpl.class)//必须要加  https://www.cnblogs.com/blog5277/p/10661441.html
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class);
    }

    @Bean
    @LoadBalanced //ribbon的负载均衡
    public RestTemplate restTemplate(){

        return new RestTemplate();
    }

}
