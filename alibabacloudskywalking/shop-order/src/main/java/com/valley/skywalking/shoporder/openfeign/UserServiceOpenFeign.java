package com.valley.skywalking.shoporder.openfeign;

import com.valley.skywalking.shopcommon.domain.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value="shop-user")
public interface UserServiceOpenFeign {
    @RequestMapping("/user/info/{userid}")
    public User userInfo(@PathVariable("userid") String userid);
}
