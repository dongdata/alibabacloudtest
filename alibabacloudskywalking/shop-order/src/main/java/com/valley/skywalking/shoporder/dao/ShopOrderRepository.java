package com.valley.skywalking.shoporder.dao;


import com.valley.skywalking.shopcommon.dao.BaseDao;
import com.valley.skywalking.shopcommon.domain.ShopOrder;

import java.io.Serializable;

/**
 * @author Administrator
 */
public interface ShopOrderRepository extends BaseDao<ShopOrder, Serializable> {
}
