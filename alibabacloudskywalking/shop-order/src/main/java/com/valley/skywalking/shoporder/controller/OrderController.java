package com.valley.skywalking.shoporder.controller;

import com.alibaba.fastjson.JSON;
import com.valley.skywalking.security.service.LoginService;
import com.valley.skywalking.security.utils.ServletUtils;
import com.valley.skywalking.shopcommon.domain.ShopOrder;
import com.valley.skywalking.shopcommon.domain.Product;
import com.valley.skywalking.shopcommon.domain.User;
import com.valley.skywalking.shopcommon.page.PageDomain;
import com.valley.skywalking.shopcommon.page.TableDataInfo;
import com.valley.skywalking.shoporder.openfeign.OrderServiceOpenFeign;
import com.valley.skywalking.shoporder.openfeign.UserServiceOpenFeign;
import com.valley.skywalking.shoporder.service.ShopOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {
//    @Autowired
//    private RestTemplate restTemplate;

    @Autowired
    private ShopOrderService orderService;


    @Autowired
    /**
     * 要加@ComponentScan
     * */
    private LoginService loginService;

    @Autowired
    private OrderServiceOpenFeign orderServiceOpenFeign;

    @Autowired
    private UserServiceOpenFeign userServiceOpenFeign;

    @GetMapping
    public TableDataInfo all(ShopOrder shopOrder, PageDomain pageDomain){
        return orderService.tableData(shopOrder, pageDomain);
    }

    @PostMapping("/prod")
    public List<ShopOrder> orders(@RequestBody List<Integer> pids){
        if(pids == null || pids.size() == 0){
            return null;
        }
        User user = getUser();
        if (user == null) {
            return null;
        }
        List<ShopOrder> shopOrders = new ArrayList<>();
        Date now = new Date();
        for(Integer pid : pids) {
            log.info("接收到{}号商品的下单请求，调用商品微服务查询此商品信息", pid);
            Product product = orderServiceOpenFeign.product(pid);

            log.info("查询到{}号商品的信息，内容:{}", pid, JSON.toJSONString(product));
            //下单
            ShopOrder order = new ShopOrder();

            order.setUid(user.getUid());
            order.setUsername(user.getUsername());

            order.setPid(pid);
            order.setPname(product.getPname());
            order.setPprice(product.getPprice());
            order.setNumber(1);
            order.setOrderdate(now);

            orderService.createOrder(order);

            log.info("创建{}订单成功", pid);
            shopOrders.add(order);
        }
        return shopOrders;

    }

    private User getUser() {
        String uid = loginService.getLoginUserid(ServletUtils.getRequest());
        if(uid == null || uid.isEmpty()){
            return null;
        }
        return userServiceOpenFeign.userInfo(uid);
    }


    @GetMapping("/prod/{pid}")
    public ShopOrder order(@PathVariable("pid") Integer pid){
        log.info("接收到{}号商品的下单请求，调用商品微服务查询此商品信息",pid);

//        Product product
//                = restTemplate.getForObject("http://shop-product/product/" + pid, Product.class);
        User user = getUser();
        if (user == null) {
            return null;
        }
        Product product = orderServiceOpenFeign.product(pid);

        log.info("查询到{}号商品的信息，内容:{}",pid, JSON.toJSONString(product));
        //下单
        ShopOrder order = new ShopOrder();

        user.setUid(user.getUid());
        order.setUsername(user.getUsername());

        order.setPid(pid);
        order.setPname(product.getPname());
        order.setPprice(product.getPprice());
        order.setNumber(1);

        orderService.createOrder(order);

        log.info("创建{}订单成功",pid);

        return order;

    }

    @DeleteMapping("/prod")
    public Integer deleteOrders(@RequestBody List<Long> pids){
        if(pids == null || pids.size() == 0){
            return -1;
        }
        User user = getUser();
        if (user == null) {
            return -1;
        }
        for(Long pid : pids) {
            log.info("取消{}号商品的下单请求", pid);
            orderService.deleteOrder(pid);
            log.info("取消{}号订单成功", pid);
        }
        return 0;

    }

}
