package com.valley.skywalking.shopuser.service;

import com.valley.skywalking.shopcommon.domain.User;

public interface UserService {
    public User findById(Integer userid);
    public User findByUsername(String username);
    public int save(User user);
}
