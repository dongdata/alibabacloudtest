package com.valley.skywalking.shopuser.dao;

import com.valley.skywalking.shopcommon.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Administrator
 */
public interface UserDao extends JpaRepository<User,Integer> {
    Optional<User> findUserByUsername(String username);
}
