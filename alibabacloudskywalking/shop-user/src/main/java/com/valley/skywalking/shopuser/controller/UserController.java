package com.valley.skywalking.shopuser.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.valley.skywalking.shopcommon.domain.User;
import com.valley.skywalking.shopuser.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public int login(@RequestBody  User user){
        if(user.getUsername() == null || user.getUsername().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty()){
            return -1;
        }
        User user1 = userService.findByUsername(user.getUsername());
        if (user1.getPassword() == null){
            log.error(String.format("数据库中 %s 密码为空", user.getUsername()));
            return -1;
        }else if(!user1.getPassword().equals(user.getPassword())){
            log.error(String.format(" %s 密码尝试错误！", user.getUsername()));
            return -1;
        }
        return 0;
    }

    @PostMapping("/info")
    public int userInfo(@RequestBody  User user){
        return userService.save(user);
    }

    @GetMapping("/info/{userid}")
    public User userInfo(@PathVariable Integer userid){
        return userService.findById(userid);
    }

    @GetMapping("/testFlow")
    @SentinelResource(value = "user-testFlow",
            blockHandlerClass = UserBlockHandler.class,
            blockHandler = "handleException",//只负责sentinel控制台配置违规
            fallback = "handleError", //只负责业务异常
            fallbackClass = UserBlockHandler.class
    )
    public String testFlow(){
        return JSON.toJSONString(userService.findById(1));
    }
    @GetMapping("/testDegrade")
    @SentinelResource(value="user-testDegrade",
            blockHandlerClass = UserBlockHandler.class, //对应异常类
            blockHandler = "handleException", //只负责sentinel控制台违规
            fallback = "handleError", //只负责业务异常
            fallbackClass = UserBlockHandler.class)
    public String testDegrade(){
        return JSON.toJSONString(userService.findById(1));
    }
}
