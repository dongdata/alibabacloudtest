package com.valley.skywalking.shopuser.service.impl;

import com.valley.skywalking.shopcommon.domain.User;
import com.valley.skywalking.shopuser.dao.UserDao;
import com.valley.skywalking.shopuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;


    @Override
    public User findById(Integer userid) {
        Optional<User> userOptional =  userDao.findById(userid);
        return userOptional.orElse(null);
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> userOptional =  userDao.findUserByUsername(username);
        return userOptional.orElse(null);
    }

    @Override
    public int save(User user) {
         userDao.save(user);
         return 0;
    }
}
