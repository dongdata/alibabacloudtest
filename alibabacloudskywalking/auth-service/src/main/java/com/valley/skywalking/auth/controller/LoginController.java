package com.valley.skywalking.auth.controller;

import com.valley.skywalking.auth.request.RefreshRequest;
import com.valley.skywalking.auth.service.UserService;
import com.valley.skywalking.security.response.LoginResponse;
import com.valley.skywalking.security.response.ResponseCodeEnum;
import com.valley.skywalking.security.response.ResponseResult;
import com.valley.skywalking.security.utils.JWTUtil;
import com.valley.skywalking.shopcommon.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/auth")
@Slf4j
public class LoginController {
    @Value("${secretKey:123456}")
    private String secretKey;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    UserService userService;

    final static String TOKEN = "token";

    final static String REFRESH_TOKEN = "refreshToken";

    @PostMapping("/login")
    public ResponseResult login(@RequestBody @Validated User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseResult.error(ResponseCodeEnum.PARAMETER_ILLEGAL.getCode(), ResponseCodeEnum.PARAMETER_ILLEGAL.getMessage());
        }
        if(user.getUsername() == null || user.getUsername().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty()){
            return ResponseResult.error(ResponseCodeEnum.LOGIN_ERROR.getCode(), ResponseCodeEnum.LOGIN_ERROR.getMessage());
        }
        User user1 = userService.findByUsername(user.getUsername());
        if(user1 == null){
            return ResponseResult.error(ResponseCodeEnum.LOGIN_ERROR.getCode(), ResponseCodeEnum.LOGIN_ERROR.getMessage());
        }
        if (user1.getPassword() == null){
            log.error(String.format("数据库中 %s 密码为空", user.getUsername()));
            return ResponseResult.error(ResponseCodeEnum.LOGIN_ERROR.getCode(), ResponseCodeEnum.LOGIN_ERROR.getMessage());
        }else if(!user1.getPassword().equals(user.getPassword())){
            log.error(String.format(" %s 密码尝试错误！", user.getUsername()));
            return ResponseResult.error(ResponseCodeEnum.LOGIN_ERROR.getCode(), ResponseCodeEnum.LOGIN_ERROR.getMessage());
        }
        String username = user1.getUsername();
        String password = user1.getPassword();
        //  假设查询到用户ID是1001
        String userId = user1.getUid().toString();
            //  生成Token
        String token = JWTUtil.generateToken(userId,username,secretKey);

        //  生成刷新Token
        String refreshToken = UUID.randomUUID().toString().replace("-", "");

        //  放入缓存
        HashOperations<String, String, String> hashOperations = stringRedisTemplate.opsForHash();

        String key = userId;
        hashOperations.put(key, TOKEN, token);
        hashOperations.put(key, REFRESH_TOKEN, refreshToken);
        stringRedisTemplate.expire(key, JWTUtil.TOKEN_EXPIRE_TIME, TimeUnit.MILLISECONDS);


        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setToken(token);
        loginResponse.setRefreshToken(refreshToken);
        loginResponse.setUserId(userId);
        loginResponse.setUsername(username);

        return ResponseResult.success(loginResponse);

    }

    @PostMapping("/logout")
    public ResponseResult logout(@RequestBody String token) {
        HashOperations<String, String, String> hashOperations = stringRedisTemplate.opsForHash();
        if(token == null || token.isEmpty()){
            return ResponseResult.success();
        }
        String key = JWTUtil.getUserId(token);
        if(key != null && !key.isEmpty()){
            hashOperations.delete(key, TOKEN);
        }
        return ResponseResult.success();
    }

    @PostMapping("/refreshToken")
    public ResponseResult refreshToken(@RequestBody @Validated RefreshRequest request, BindingResult bindingResult) {
        String userId = request.getUserId();
        //通过userId去数据库查到userName
        User user = userService.findById(Integer.valueOf(userId));
        String userName=user.getUsername();
        String refreshToken = request.getRefreshToken();
        HashOperations<String, String, String> hashOperations = stringRedisTemplate.opsForHash();
        String key = userId;
        String originalRefreshToken = hashOperations.get(key, REFRESH_TOKEN);
        if (StringUtils.isBlank(originalRefreshToken) || !originalRefreshToken.equals(refreshToken)) {
            return ResponseResult.error(ResponseCodeEnum.REFRESH_TOKEN_INVALID.getCode(), ResponseCodeEnum.REFRESH_TOKEN_INVALID.getMessage());
        }

        //  生成新token
        String newToken = JWTUtil.generateToken(userId,userName,secretKey);
        hashOperations.put(key, TOKEN, newToken);
        stringRedisTemplate.expire(userId, JWTUtil.TOKEN_EXPIRE_TIME, TimeUnit.MILLISECONDS);

        return ResponseResult.success(newToken);
    }
}
