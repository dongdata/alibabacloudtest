package com.valley.skywalking.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import javax.persistence.Entity;

/**
 * @author Administrator
 */
@SpringBootApplication
@EnableDiscoveryClient
@EntityScan("com.valley.skywalking.shopcommon.domain")
public class AuthServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }
}
