package com.valley.skywalking.shopproduct.service.impl;

import com.valley.skywalking.shopcommon.domain.Product;

import java.util.List;

public interface ProductService {
    Product findById(Integer pid);
    List<Product> findAll();
}
