package com.valley.skywalking.shopproduct.dao;

import com.valley.skywalking.shopcommon.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDao extends JpaRepository<Product,Integer> {
}
