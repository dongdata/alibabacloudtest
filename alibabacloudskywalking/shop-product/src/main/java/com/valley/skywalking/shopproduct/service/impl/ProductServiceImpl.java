package com.valley.skywalking.shopproduct.service.impl;

import com.valley.skywalking.shopcommon.domain.Product;
import com.valley.skywalking.shopproduct.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    private ProductDao productDao;
    @Override
    public Product findById(Integer pid) {
        return productDao.findById(pid).get();
    }

    @Override
    public List<Product> findAll() {
        return productDao.findAll();
    }
}
