import Login from "@/components/Login.vue";
import  VueRouter  from 'vue-router';
import Vue from "vue";
import Layout from "@/components/layout";
import ClientMonitor from 'skywalking-client-js'
import {getToken} from "@/utils/auth";
const whiteList = ['/login', '/auth-redirect', '/bind', '/register']
Vue.use(VueRouter);

export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    hidden: true,
    children: [
      {
        name: "maincontent",
        path: 'product',
        components: {
          maincontent: ()=> import('@/views/Product')
      }
      },
      {
        name: "maincontent",
        path: 'order',
        components: {
          maincontent: ()=> import('@/views/Order')
      }
      }
    ]
  },
  // { path: '/', component: Product },
  { path: '/login', component: Login },

]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: constantRoutes
})
router.beforeEach((to, from, next) => {
  console.log(to, from)
  if (getToken()) {
    if (to.path === '/login') {
      next()
      // NProgress.done()
    }else{
      next()
    }
  }else{
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next(`/login`) // 否则全部重定向到登录页
      // NProgress.done()
    }
  }
})
router.afterEach(() => {
  // NProgress.done();
  ClientMonitor.setPerformance({ service: '购物系统', serviceVersion: '8.9', pagePath: location.href, useFmp: true, vue: 'Vue' })
})
export default router
