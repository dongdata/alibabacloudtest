import request from "@/utils/request";

export function allProduct(){
    return request({
        url: '/product',
        method: 'get',
    })
}


export function order(pids){
    return request({
        url: '/order/prod',
        method: 'post',
        data: pids
    })
}


export function orderList(params){
    return request({
        url: '/order',
        method: 'get',
        params: params
    })
}


export function deleteOrder(pids){
    return request({
        url: '/order/prod',
        method: 'delete',
        data: pids
    })
}