import request from "@/utils/request";

export function login(form){
    return request({
        url: '/auth/login',
        method: 'post',
        data: form
    })
}



// 获取用户详细信息
export function getInfo() {
    return request({
        url: '/getInfo',
        method: 'get'
    })
}

// 退出方法
export function logout(token) {
    return request({
        url: '/auth/logout',
        method: 'post',
        data: token
    })
}