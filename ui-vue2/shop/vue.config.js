module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        open: true,
        proxy: {
            ['/api/*']:{
                target: 'http://192.168.10.200:8002',
                changeOrigin: true,
                pathRewrite: {
                    ['^/api']: ''
                }
            },
            '/browser': {
                target: 'http://192.168.10.203:12800',
                changeOrigin: true
            }
        },
        disableHostCheck: true
    },
    css: {
        loaderOptions: {
            sass: {
                sassOptions: { outputStyle: "expanded" }
            }
        }
    },
}